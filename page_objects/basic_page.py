from config.android_config import AndroidConfig


class BasicPage:

    def __init__(self, driver=None):
        if driver is None:
            driver = AndroidConfig.init_mobile_app()
        self.driver = driver

    def quit(self):
        self.driver.quit()

