from page_objects.basic_page import BasicPage
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from common.webutil import is_element_visible, wait_for_element_visible
from common import logger

main_page_footer = (By.ID, 'main_footer_container_fl')
automatic_cash_back = (By.ID, 'viewthrough_maybe_later_b')


class MainPage(BasicPage):

    def is_main_page_footer_visible(self):
        logger.info("checking if main page footer is visible")
        try:
            wait_for_element_visible(self.driver, automatic_cash_back, timeout=10)
        except TimeoutException:
            logger.info("Cashback didn't appear")
        if is_element_visible(self.driver, *automatic_cash_back):
            self.driver.find_element(*automatic_cash_back).click()
        return is_element_visible(self.driver, *main_page_footer)
