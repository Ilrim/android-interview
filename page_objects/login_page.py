from page_objects.basic_page import BasicPage
from selenium.webdriver.common.by import By
from common.webutil import wait_for_element_visible, clear_and_input
from common import logger
from page_objects.main_page import MainPage

login_adv_text = (By.ID, 'login_text_tv')
email_signup_menu = (By.ID, 'login_sing_up_email_b')
email_field = (By.ID, 'sign_up_email_edit_et')
password_field = (By.ID, 'sign_up_password_edit_et')
create_account_btn = (By.ID, 'sign_up_email_b')
account_details_create_account = (By.ID, 'account_details_create_account_b')
popup_notification = (By.ID, 'action_bar_root')
message = (By.ID, 'message')
diesel_fuel = (By.ID, 'diesel')


class LoginPage(BasicPage):
    def get_adv_text(self):
        return wait_for_element_visible(self.driver, login_adv_text).text

    def click_sign_up_email_option(self):
        logger.info("Pressing Sign up with email")
        wait_for_element_visible(self.driver, email_signup_menu).click()

    def enter_credentials(self, login, pwd):
        logger.info("Enter credentials for login")
        clear_and_input(self.driver, email_field, login)
        clear_and_input(self.driver, password_field, pwd)
        self.driver.hide_keyboard()
        wait_for_element_visible(self.driver, create_account_btn).click()

    def get_same_account_warning(self):
        wait_for_element_visible(self.driver, popup_notification)
        return wait_for_element_visible(self.driver, message).text

    def select_fuel_type(self, fuel_type):
        if fuel_type == 'diesel':
            logger.info("selecting fuel type {}".format(fuel_type))
            wait_for_element_visible(self.driver, diesel_fuel).click()
            wait_for_element_visible(self.driver, account_details_create_account).click()
            return MainPage(driver=self.driver)
        else:
            raise NotImplementedError("haven't implemented for this fuel type: {}".format(fuel_type))
