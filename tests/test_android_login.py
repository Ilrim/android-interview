import json
import uuid
import random
import string
from page_objects.login_page import LoginPage
from common.base_test import BaseTest

warning_the_same_account = 'Oops. This email is already taken. Please choose different email or reset your password.'

with open('config/accounts.json') as f:
    account = json.load(f)


class MobileAppTestAndroid(BaseTest):

    def test_sign_up_email(self):
        fuel_type = 'diesel'
        acc = str(uuid.uuid4())[:7] + '@gm.com'
        password = ''.join(random.choice(string.printable) for i in range(8)) + random.choice(string.digits)
        login_page = LoginPage()
        assert 'Get cash back on gas' in login_page.get_adv_text()
        login_page.click_sign_up_email_option()
        login_page.enter_credentials(acc, password)
        main_page = login_page.select_fuel_type(fuel_type)
        assert main_page.is_main_page_footer_visible() is True

    def test_same_account_sign_up(self):
        login_page = LoginPage()
        login_page.click_sign_up_email_option()
        login_page.enter_credentials(account['email'], account['password'])
        assert warning_the_same_account == login_page.get_same_account_warning()
