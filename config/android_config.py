import os
from appium import webdriver as appium_wd
from common import logger
from common.constants import APP_NAME, APP_PATH
from urllib.error import URLError
from selenium.common.exceptions import WebDriverException


class AndroidConfig:
    _all_drivers = []
    _all_session_ids = []
    _driver = None
    test_name = None

    @classmethod
    def startup_mobile_application_in_local(cls, caps, options=None):
        cls._driver = appium_wd.Remote(desired_capabilities=caps,
                                       browser_profile=options,
                                       command_executor='http://127.0.0.1:4723/wd/hub')

    @classmethod
    def init_mobile_app(cls):
        caps = {'deviceName': 'Nexus 6',
                'avd': "Nexus_6",
                'platformName': 'Android',
                'platformVersion': '9.0',
                'appiumVersion': "1.13.0",
                'deviceOrientation': 'portrait',
                'app': os.path.join(os.getcwd(), APP_PATH, APP_NAME),
                "autoGrantPermissions": "true"}

        logger.info('Starting local tests run')
        cls.startup_mobile_application_in_local(caps)
        cls._all_drivers.append(cls._driver)
        cls._all_session_ids.append(cls._driver.session_id)

        return cls._driver

    @classmethod
    def cleanup(cls):
        for running_driver in cls._all_drivers:
            try:
                running_driver.quit()
            except (WebDriverException, URLError) as e:
                logger.warning(e)
        # reset
        cls._all_drivers = []
        cls._driver = None
        cls._all_session_ids = []
        cls.test_name = None
