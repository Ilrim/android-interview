import unittest
from config.android_config import AndroidConfig
from common import logger


class BaseTest(unittest.TestCase):

    def setUp(self):
        logger.info('Base Test Setup')
        AndroidConfig.test_name = self.id()
        logger.info("===== Begin test: %s  =====", self.id())

    def tearDown(self):
        logger.info('Base Test Teardown')
        AndroidConfig.cleanup()
        logger.info("===== End test: %s  =====", self.id())
