from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

HTTP_TIMEOUT = 30


def wait_for_element_visible(driver, locator, timeout=HTTP_TIMEOUT):
    return WebDriverWait(driver, timeout, ignored_exceptions=[StaleElementReferenceException]).until(
        expected_conditions.visibility_of_element_located(locator))


def clear_and_input(driver, locator, value):
    element = wait_for_element_visible(driver, locator)
    element.clear()
    element.send_keys(value)


def is_element_visible(driver, how, what, wait=False):
    try:
        if wait:
            wait_for_element_visible(driver, (how, what))
        element = driver.find_element(by=how, value=what)
        if element.is_displayed():
            return True
        return False
    except (NoSuchElementException, TimeoutException):
        return False
