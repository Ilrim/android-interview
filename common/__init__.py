# tests are runing without xdist
import datetime
import logging

logging.basicConfig(filename="Log_File.log", level=logging.INFO)

# create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.name = str(datetime.datetime.now().microsecond)
ch.setLevel(logging.INFO)
# create formatter
formatter = logging.Formatter(ch.name + " %(asctime)s - %(filename)s - %(levelname)s - %(message)s")
# add formatter to ch
ch.setFormatter(formatter)
# add ch to logger
logger.addHandler(ch)
