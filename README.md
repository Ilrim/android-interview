## Get Started
1. Install Appium and Android studio
2. Start Appium 
3. Tests were created with Android 9 and Nexus 6 device, so probably have either to install the same 
environment or update android_config.py file with your device name, Appium version and Android version
4. Start mobile device in AVD
5. Add app-betabs-debug.apk file to application folder
6. Run a command from use cases below to start a test


## Use Cases

* Run all tests:

```bash
$ py.test -v -s tests

```
** Run single test
```bash
$  py.test -v -s tests -k test_sign_up_email


```